using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

using MimeTypes;

namespace Uploader.Controllers
{
    [Route("/")]
    public class ViewController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public ViewController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("{filename}")]
        public IActionResult View(string filename)
        {
            string webroot = _hostingEnvironment.WebRootPath;
            string filePath = Path.Combine(webroot, filename);

            if (System.IO.File.Exists(filePath))
            {
                Stream stream = System.IO.File.OpenRead(filePath);
                string fileExt = filename.Split('.').Last();

                return File(stream, MimeTypeMap.GetMimeType(fileExt));
                             
            } else {
                return NotFound();
            }
        }
    }
}