using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Uploader.Models;

namespace Uploader.Controllers
{
    [Route("api/[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        private readonly FileContext _fileContext;

        private static Random random = new Random();

        public UploadController(IHostingEnvironment hostingEnvironment, IConfiguration configuration, FileContext fileContext)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _fileContext = fileContext;
        }

        [HttpPost]
        public IActionResult Upload()
        {
            KeyValuePair<string, StringValues> authHeader = Request.Headers.Where(header => header.Key.ToString().ToLower() == "auth").First();

            if (null == _configuration["Settings:UploadSecret"])
            {
                _configuration["Settings:UploadSecret"] = "bashiscool";
            }

            if (authHeader.Value == _configuration["Settings:UploadSecret"])
            {
                string webroot = _hostingEnvironment.WebRootPath;

                IFormFile file = Request.Form.Files.First();
                string filename = file.FileName;
                Stream stream = file.OpenReadStream();

                int filenameLength = Convert.ToInt32(_configuration["Settings:FilenameLength"]);
                int secretLength = Convert.ToInt32(_configuration["Settings:SecretLength"]);

                string randFilename = RandomString(filenameLength);
                string fileExt = filename.Split('.').Last();

                string newFilename = randFilename + "." + fileExt;

                string filePath = Path.Combine(webroot, randFilename + "." + fileExt);

                while (System.IO.File.Exists(filePath))
                {
                    randFilename = RandomString(filenameLength);
                    newFilename = randFilename + "." + fileExt;
                    filePath = Path.Combine(webroot, newFilename);
                }                

                using (System.IO.Stream s = System.IO.File.Create(filePath))
                {
                    stream.CopyTo(s);
                }

                string deletionSecret = RandomString(secretLength);

                _fileContext.Add(new FileRecord() { Filename = newFilename, Secret = deletionSecret });
                _fileContext.SaveChanges();

                stream.Close();

                return Ok(new { filename = newFilename, deletionSecret = deletionSecret });
                
            } else {
                return Unauthorized();
            }
        }

        private static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}